<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  // fillableの設定をすることによって、予期せぬ代入が起こることを防ぐ。
  protected $fillable = ['title', 'content', 'user_id', 'image_url'];
}
