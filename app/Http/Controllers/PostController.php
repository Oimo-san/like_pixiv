<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// ログインやらPostクラスやらを読み込んでおく
use App\Post;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreatePost;

class PostController extends Controller
{

  // 投稿時、投稿された内容をDBに登録する。
  // 画像ファイル名をリネームし、storeAs関数を利用して「storage/post_images」に保存するように設定。
  public function create(Request $request)
  {
    $time = date("Ymdhis"); // 時間取得

    $post = new Post();
    $post->title = $request->title;
    $post->content = $request->content;
    $post->user_id = Auth::user()->id; // 登録ユーザーからidを取得
    if($request->image_url !== null)
    {
      $post->image_url = $request->image_url->storeAs('public/post_images', $time.'_'.Auth::user()->id . '.jpg');
      $post->save();  // インスタンスの状態をデータベースに書き込む
    }
    
    // $this->toHome();ではどうしてダメ？
    $posts = Post::latest()->get();
    return view('home')->with('posts', $posts);
  }

  // ホーム画面への遷移
  public function toHome()
  {
    // $posts = Post::orderBy('created_at', 'desc')->get();と同じ意味
    $posts = Post::latest()->get();

    // データが取得できたかどうかは、 Laravel で用意されている dd() という命令を使って確かめられる
    // dd($posts->toArray()); // dump die の略

    // $postsのデータをviewの方に渡すには、view とした後に view の名前を指定してあげる。
    // ここでフォルダの区切りは .(ドット) になっている点に注意。
    // return view('home', ['posts' => $posts]); と同じ意味。
    return view('home')->with('posts', $posts);
  }

  // ポストの削除
  public function destoroy(Post $post)
  {
    $post->delete();
    $posts = Post::latest()->get();
    return view('home')->with('posts', $posts);
  }

}
