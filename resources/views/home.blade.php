@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {{-- デバッグ用 --}}
                    {{-- ユーザー情報の確認 --}}
                    <div class="debugString">
                        <?php $user = Auth::user(); ?>
                        <div>username: {{ $user->name }}</div>
                        <div>id: {{ $user->id }}</div>
                        <div>email: {{ $user->email }}</div>
                        <div>pass: {{ $user->password }}</div>
                    </div>

                    {{-- TODO: ユーザー情報から登録した画像URLをDBから引っ張ってくる --}}
                    <div class="imagesDiv">
                      <h4>【画像一覧】</h4>
                      @forelse ($posts as $post)
                      <li>
                          <p>タイトル: {{ $post->title }}<p>
                          @if ($post->image_url)
                          <p>
                            <a href="/{{ str_replace('public/', 'storage/', $post->image_url) }}">
                              <img src ="/{{ str_replace('public/', 'storage/', $post->image_url) }}" width=25% height=auto>
                            </a>
                          </p>
                          <p>コメント:<p>
                          <pre>{{$post->content}}</pre>

                          @endif
                          <a href="{{ action('PostController@destoroy', $post) }}" class="edit">[削除](※未実装)</a>
                          <!-- <form method="post" action="{{ route('posts.delete') }}">
                              {{ csrf_field() }}
                              <input type="submit" value="削除" class="btn btn-danger btn-sm" onclick='return confirm("削除しますか？");'>
                          </form> -->



                      </li>
                      @empty
                      <li>
                          No images yet
                      </li>
                      @endforelse

                    </div>


                    <h4>【新規投稿フォーム】</h4>
                    <form method="post" action="{{ route('posts.create') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form">
                            <div class="form-title">
                                <label for="title">タイトル</label>
                                <input class="" name="title" value="{{ old('title') }}">
                            </div>

                            <div class="form-image_url">
                                <input type="file" name="image_url">
                            </div>

                            <div class="form-content">
                                <label for="content" class="form-content">内容</label>
                                <textarea class="" name="content" cols="50" rows="10">{{ old('content') }}</textarea>
                            </div>

                            <div class="form-submit">
                                <button type="submit">投稿する</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
